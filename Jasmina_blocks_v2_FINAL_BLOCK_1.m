% session prameters
imgdir = 'imgs';

%Nblocks = 3;
%NtrialsPerBlock = 30;
nEvents=20;
initBlank=8.0; % values only for testing
eventDuration=22.0;
eventInterval=34.0;
events=randperm(nEvents);
lastBlank=12.0;
imgUpdate=1.0;
initialmag=3;
magstep=0.94;

% PTB stuff
KbName('UnifyKeyNames')

%Keyboards
escapeKey = KbName('ESCAPE');
key1 = KbName('1');
key2 = KbName('2');
key3 = KbName('3');
key4 = KbName('4');


% initialising screen
% Screen('Preference', 'SkipSyncTests', 1);
v = bitor(2^16, Screen('Preference','ConserveVRAM'));
Screen('Preference','ConserveVRAM', v);
screens=Screen('Screens');

screenNumber=max(screens);
[expWin,rect]=Screen('OpenWindow',screenNumber,[128 128 128]);
[xc, yc]=RectCenter(rect);
oneFrame=Screen('GetFlipInterval',expWin, 1);
Screen('TextSize',expWin, 24);

stimList = dir([imgdir '/*.jpg']);

% load pictures: will be elaborated later
dstWidth=700;
dstHeight=700;

% setting event start-up times
eventStart=zeros([1 nEvents]);
eventEnd=zeros([1 nEvents]);

for i=1:nEvents
    eventStart(i)=initBlank+eventInterval*(i-1);
    eventEnd(i)=eventStart(i)+eventDuration;
end

%eventStart = repmat(eventStart,[1 Nblocks]);
%eventEnd = repmat(eventEnd,[1 Nblocks]);

sessionBlock=initBlank+eventInterval*nEvents+lastBlank;

% session start

try
    HideCursor
    ListenChar(2);
    for i=1:nEvents      
        
        if mod(i,nEvents) == 1
            t0=startExp(expWin);
            t99 = clock; % time stamp at the begining of the block
            starttime=now;
            logfile=['log' datestr(starttime,'yyyymmdd-HHMMSS') '.txt'];
            fid=fopen(logfile, 'w');
            fprintf(2, ['logfile = ' logfile]);
            fprintf(fid, ['Started at ' datestr(starttime,'yyyy/mm/dd HH:MM:SS') '\n']);
        end
        
        % ITI
        
        StimName = stimList(events(i)).name;
        t=GetSecs-t0;
        
        img=imread([imgdir '/' StimName], 'jpg');
        imgWidth=size(img,2);
        imgHeight=size(img,1);
        textureIndex=Screen('MakeTexture', expWin, double(img));
        
        if t<(eventStart(i)-oneFrame) % wait for the begining fo the experiment
            BlankShow(i) = etime(clock,t99) % the time of the begining of the experiment
            FlipTimestamp=showBlank(expWin);
            fprintf(fid,'%f : Blank started\n',FlipTimestamp-t0);
            
            while t<(eventStart(i)-oneFrame)
                t=GetSecs-t0;
                [keyIsDown, secs, keyCode, deltaSecs] = KbCheck(-1);
                if keyCode(escapeKey)
                    fclose('all');
                    ListenChar(0);
                    sca;
                    error('Terminated by ESC');
                end
            end
        end
        
        t=GetSecs-t0;
        %fprintf('%f : Event %d ( %s ) started\n',t, i, jpgfile{i});
        StimShow(i) = etime(clock,t99) % end of the blank, begin of the stim
        fprintf(fid,'%f\tTrial %d = image #%d\t%s\n', StimShow(i), i, events(i), StimName);
        fprintf(1,'%f\tTrial %d = image #%d\t%s\n', StimShow(i), i, events(i), StimName);
        
        while t<eventEnd(i)-oneFrame
            n=floor(t-eventStart(i));
            mag=initialmag*magstep^n;
            flipt=showImg(textureIndex, imgWidth, imgHeight, mag, xc, yc, dstWidth, dstHeight, expWin);
            [keyIsDown, secs, keyCode, deltaSecs] = KbCheck(-1);
            if keyIsDown
                if keyCode(escapeKey)               
                    ListenChar(0);
                    sca;
                    error('%f: Terminated by ESC', secs-t0);
                else
                     fprintf(fid,'%f : key %i pressed (%s)\n', secs-t0, find(keyCode), KbName(keyCode));
                     fprintf(1,'%f : key %i pressed (%s)\n', secs-t0, find(keyCode), KbName(keyCode));
                    while keyIsDown
                        [keyIsDown, secs, keyCode, deltaSecs] = KbCheck(-1);
                    end
                end
                FlushEvents;
                
            end
            t=GetSecs-t0;
        end
        
        StimEnd(i) = etime(clock,t99) % end of the stim
        %fprintf('%f : Event %d finished\n',t, i);
        
        if mod(i,nEvents) == 0
            % final blank
            t=showBlank(expWin)-t0;
            while t<sessionBlock-oneFrame
                t=GetSecs-t0;
                [keyIsDown, secs, keyCode, deltaSecs] = KbCheck(-1);
                
                if keyCode(escapeKey)
                    ListenChar(0);
                    sca;
                    error('Terminated by ESC');
                end
            end
            
            t=GetSecs-t0;
            %fprintf('%f : Session %d finished\n',t, i);
            %Screen('CloseAll');
            
            fprintf(fid, ['Finished at ' datestr(now,'yyyy/mm/dd HH:MM:SS') '\n']);
            fclose(fid);
            
        end
        
        
    end
    FlushEvents('keyDown');
    ListenChar(0);
    Screen('CloseAll');
    
catch
    Screen('CloseAll');
    fclose('all');
    psychrethrow(psychlasterror);
    
    ListenChar(0);
    Screen('CloseAll');
    
end

ShowCursor

save timedata.mat StimEnd StimShow



