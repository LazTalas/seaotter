library(ggplot2)

# set working directory
setwd('/Users/Trilby/Dropbox/Las&JasScienceBusiness')
#setwd('/Volumes/Data/jasste/Dropbox/Las&JasScienceBusiness')

window <- 13

# load tables
control <- read.csv('control_mosaic.csv', header = F)
experiment <- read.csv('experiment_aha.csv', header = F)

# transpose tables
control <- t(control)[,1:window]
experiment <- t(experiment)[,1:window]

# means

# mean_control <- colMeans(control)
# #mean_control <- mean_control / abs(max(mean_control))
# mean_experiment <- colMeans(experiment)
# #mean_experiment <- mean_experiment / abs(max(mean_experiment))
# 
# quotient_vector <- mean_experiment / mean_control
# min_qv <- min(quotient_vector)
# max_qv <- max(quotient_vector)
# 
# steps <- seq(from = min_qv, to = max_qv, by = 0.1)
# dist_score <- rep(0, times = length(steps))
# 
# for (s_idx in 1:length(steps)){
# 	current_control <- control * steps[s_idx]
# 	current_experiment <- experiment # * steps[s_idx]
# 	dist_score[s_idx] <- sqrt(sum((colMeans(current_experiment) - colMeans(current_control)) ^ 2))
# 	best_dist_idx <- which(dist_score == min(dist_score))[1]
# }

constant <- 6.5
adj_control <- control*constant

# control summary
summary_control <- as.data.frame(matrix(data = 0, nrow = dim(control)[2], ncol = 6))
colnames(summary_control) <- c('Mean','Second','Class','sd','se','ci')
summary_control[,'Mean'] <- colMeans(adj_control)
summary_control[,'Second'] <- seq(-2, dim(control)[1])
summary_control[,'Class'] <- 'Control'
summary_control[,'sd'] <- apply(adj_control, 2, sd)
summary_control[,'se'] <- apply(adj_control, 2, sd) / sqrt(dim(adj_control)[1])
summary_control[,'ci'] <- qnorm(0.975) * apply(adj_control, 2, sd)/sqrt(dim(adj_control)[1])

# experiment summary
summary_experiment <- as.data.frame(matrix(data = 0, nrow = dim(experiment)[2], ncol = 6))
colnames(summary_experiment) <- c('Mean','Second','Class','sd','se','ci')
summary_experiment[,'Mean'] <- colMeans(experiment)
summary_experiment[,'Second'] <- seq(-2, dim(control)[1])
summary_experiment[,'Class'] <- 'Experiment'
summary_experiment[,'sd'] <- apply(experiment, 2, sd)
summary_experiment[,'se'] <- apply(experiment, 2, sd) / sqrt(dim(experiment)[1])
summary_experiment[,'ci'] <- qnorm(0.975) * apply(experiment, 2, sd)/sqrt(dim(experiment)[1])

summary <- rbind(summary_control,summary_experiment)

quartz()
pd <- position_dodge(.1) # move them .05 to the left and right
ggplot(summary, aes(x=Second, y=Mean, colour=Class, group=Class)) + 
    geom_errorbar(aes(ymin=Mean-ci, ymax=Mean+ci), colour="black", width=.1, position=pd) +
    geom_line(position=pd) +
    geom_point(position=pd, size=3)

#quartz()
#plot(colMeans(adj_control), type = 'l', col = 'green')
#lines(colMeans(experiment), type = "l", col = 'red')
#
## normalise participant data then differentiate it
#
#if (dim(control)[2] != dim(experiment)[2]){ # validate data
#	paste('Error! Time lengths of control and experimental condition are not equal.')
#} else {
#	norm_control <- matrix(data = 0, nrow = dim(control)[1], ncol = dim(control)[2])
#	diff_control <- matrix(data = 0, nrow = dim(control)[1], ncol = dim(control)[2]-1)
#	norm_experiment <- matrix(data = 0, nrow = dim(experiment)[1], ncol = dim(experiment)[2])
#	diff_experiment <- matrix(data = 0, nrow = dim(experiment)[1], ncol = dim(experiment)[2]-1)
#
#
#	for (p_idx in 1:dim(control)[1]){
#		norm_control[p_idx,] <- control[p_idx,] / abs(max(control[p_idx,])) # we use the absolute value, because the maximum could be negative
#		diff_control[p_idx,] <- diff(norm_control[p_idx,])
#	}
#
#	for (p_idx in 1:dim(experiment)[1]){
#		 norm_experiment[p_idx,] <- experiment[p_idx,] / abs(max(experiment[p_idx,])) # we use the absolute value, because the maximum could be negative
#		 diff_experiment[p_idx,] <- diff(norm_experiment[p_idx,])
#	}
#}
#
## test differences
#p_vector <- rep(0,dim(diff_control)[2])
#
#for (t_idx in 1:dim(diff_control)[2]){
#	test <- t.test(diff_control[,t_idx],diff_experiment[,t_idx])
#	p_vector[t_idx] <- test$p.value # store p-value in vector
#}
#
#quartz()
#plot(colMeans(norm_control), type = 'l', main = "Mean of normalised control")
#
#quartz()
#plot(colMeans(norm_experiment), type = 'l', main = "Mean of normalised experiment")
#
#quartz()
#plot(colMeans(control), type = 'l', main = "Mean of control")
#
#quartz()
#plot(colMeans(experiment), type = 'l', main = "Mean of experiment")
#
#
#
#
#
#quartz()
#plot(colMeans(diff_control), type = 'l')
#
#quartz()
#plot(colMeans(diff_experiment), type = 'l')#