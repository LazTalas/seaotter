% Swap the energy and phase of images from 2 folders. Resize / crop images if necessary (to get a square image).

required_size = 680; % in pixels
plotme_images = 1;
plotme_freq = 1;

% get home directory (cross-platform)
if ispc
  home = [getenv('HOMEDRIVE') getenv('HOMEPATH')];
else
  home = getenv('HOME');
end

% add additional toolboxes to path
addpath(genpath([home '/Dropbox/Las&JasScienceBusiness/Matlab/toolboxes/SHINEtoolbox']));

% image folders
B_folder = [home '/Dropbox/Las&JasScienceBusiness/Studies/EnergyPhasePPA/images/faces'];
A_folder = [home '/Dropbox/Las&JasScienceBusiness/Studies/EnergyPhasePPA/images/natural-scenes'];

% create image databases
filetype='jpg'; % Change this to the appropriate file type (jpg, tif, bmp, etc.)
cd(A_folder); % Jump into folder
db_A = dir([A_folder, '/*.', filetype]); % create database A
cd(B_folder); % Jump into folder
db_B = dir([B_folder, '/*.', filetype]); % create database B

% generate image pairs
for m = 1:length(db_A)
  % prepare image A
  im_A = imread([A_folder '/' db_A(m).name]); % read in image A
    imsize_A = size(im_A); % get size dimensions
    if length(imsize_A) == 3
      im_A = rgb2gray(im_A); % make it grayscale
    end

    if imsize_A(1) ~= required_size | imsize_A(2) ~= required_size
      if imsize_A(1) == imsize_A(2) % square image
          im_A = imresize(im_A,[required_size required_size]);
        else
          [~,smaller_dim] = min(imsize_A(1:2));
          if smaller_dim == 1
              im_A = imresize(im_A,required_size/imsize_A(1)); % resize image
                imsize_A = size(im_A); % update image dimensions
                im_A = im_A(:,(floor(imsize_A(2)/2)-required_size/2):(floor(imsize_A(2)/2)+required_size/2-1)); % crop the middle
            else
                im_A = imresize(im_A,required_size/imsize_A(2)); % resize image
                imsize_A = size(im_A); % update image dimensions
                im_A = im_A(:,(floor(imsize_A(1)/2)-required_size/2):(floor(imsize_A(1)/2)+required_size/2-1)); % crop the middle
            end
        end
    end

    % Fourier transform image A
    fftA = fft2(im_A);

  for n = 1:length(db_B)
    % prepare image B
    im_B = imread([B_folder '/' db_B(n).name]); % read in image B
    imsize_B = size(im_B); % get size dimensions
    if length(imsize_B) == 3
    im_B = rgb2gray(im_B); % make it grayscale
    end

    if imsize_B(1) ~= required_size | imsize_B(2) ~= required_size
      if imsize_B(1) == imsize_B(2) % square image
          im_B = imresize(im_B,[required_size required_size]);
        else
          [~,smaller_dim] = min(imsize_B(1:2));
          if smaller_dim == 1
                im_B = imresize(im_B,required_size/imsize_B(1)); % resize image
                imsize_B = size(im_B); % update image dimensions
                im_B = im_B(:,(floor(imsize_B(2)/2)-required_size/2):(floor(imsize_B(2)/2)+required_size/2-1)); % crop the middle
            else
                im_B = imresize(im_B,required_size/imsize_B(2)); % resize image
                imsize_B = size(im_B); % update image dimensions
                im_B = im_B(:,(floor(imsize_B(1)/2)-required_size/2):(floor(imsize_B(1)/2)+required_size/2-1)); % crop the middle
            end
        end
    end

    % Fourier transform image B
    fftB = fft2(im_B);

    % swap energy and phase
    fftAB = abs(fftA).*exp(1i*angle(fftB));
    fftBA = abs(fftB).*exp(1i*angle(fftA));

    % inverse Fourier transform
    im_AB = ifft2(fftAB);
    im_BA = ifft2(fftBA);

    % normalise range
    im_AB = abs(im_AB);
    im_AB = im_AB / max(max(im_AB));

    im_BA = abs(im_BA);
    im_BA = im_BA / max(max(im_BA));

    % optional images plots
    if plotme_images
      figure(1)
      subplot(2,2,1)
      imagesc(im_A)
      colormap(gray)
      title('image A')
      subplot(2,2,2)
      imagesc(im_AB)
      colormap(gray)
      title('energy A, phase B')
      subplot(2,2,3)
      imagesc(im_BA)
      colormap(gray)
      title('energy B, phase A')
      subplot(2,2,4)
      imagesc(im_B)
      colormap(gray)
      title('image B')

      pause
      if ~plotme_freq
        close(1)
      end
    end

    % optional rotational frequency plots
    if plotme_freq
      figure(2)
      subplot(2,2,1)
      avg = sfPlot(im_A,0)
      loglog(1:floor(min(required_size,required_size)/2),avg);
      avg = avg / max(avg);
      P = polyfit(log(1:floor(required_size/2))',log(avg), 1);
    xlabel('Spatial frequency (cycles/image)');
    ylabel('Energy');
      title(num2str(P(1)))
      subplot(2,2,2)
      avg = sfPlot(im_AB,0)
      loglog(1:floor(min(required_size,required_size)/2),avg);
      avg = avg / max(avg);
      P = polyfit(log(1:floor(required_size/2))',log(avg), 1);
    xlabel('Spatial frequency (cycles/image)');
    ylabel('Energy');
      title(num2str(P(1)))
      subplot(2,2,3)
      avg = sfPlot(im_BA,0)
      loglog(1:floor(min(required_size,required_size)/2),avg);
      avg = avg / max(avg);
      P = polyfit(log(1:floor(required_size/2))',log(avg), 1);
    xlabel('Spatial frequency (cycles/image)');
    ylabel('Energy');
      title(num2str(P(1)))
      subplot(2,2,4)
      avg = sfPlot(im_B,0)
      loglog(1:floor(min(required_size,required_size)/2),avg);
      avg = avg / max(avg);
      P = polyfit(log(1:floor(required_size/2))',log(avg), 1);
    xlabel('Spatial frequency (cycles/image)');
    ylabel('Energy');
      title(num2str(P(1)))

      pause
      close all
    end

  end

end


